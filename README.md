# o3d_1-12_bull_3_hinge

Extended 2nd base project for CMU students studying Game Engine Development.

This one combines Ogre3d (1.12) and Bullet (3.0) adding a hinge constraint from Bullet to two blocks, this is one of the various constraints provided by the Bullet library. 
